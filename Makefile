CC = /usr/bin/gcc
CFLAGS = -Wall -Werror -Wextra
LDFLAGS =
SRCDIR = src/
TARGETS = $(SRCDIR)main.c $(SRCDIR)performConnection.c $(SRCDIR)sharedMemory.c $(SRCDIR)messageParser.c $(SRCDIR)stateHandler.c $ $(SRCDIR)config.c $(SRCDIR)helper.c $(SRCDIR)thinker.c

all: sysprak-client

sysprak-client: $(TARGETS)
	$(CC) -o $@ $(TARGETS) $(CFLAGS) $(LDFLAGS)

play: sysprak-client
	./sysprak-client -g $(GAME_ID) -p $(PLAYER)

clean:
	rm -f sysprak-client
