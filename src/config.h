#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>



typedef struct
{
    char *host;
    int port;
    char *type;
    int max_player_count;
} config;

config fetchConfig();