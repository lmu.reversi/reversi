#ifndef STATE_MACHINE_H
#define STATE_MACHINE_H

enum STATE {P_WELCOME, P_CLIENT, P_GAMEKIND, P_GAMENAME,
		P_YOU, P_TOTAL, P_READY, P_ENDPLAYERS, G_IDLE, G_WAIT, G_MOVE,
		G_FIELD, G_LINE, G_ENDFIELD, G_OKTHINK, G_GAMEOVER, G_P0WON,
		G_P1WON, G_QUIT, M_MOVEOK, NEGATIVE};

extern enum STATE state;

int getCurrentState();
void updateState(int state);
int getPreviousState();

#endif