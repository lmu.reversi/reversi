

struct InfoStruct
{
	char* game_id;
	char* version;
    char* player_num;
    char* next_move;
};


enum numberwang
{
    NON_NUMERIC,
    INTEGER,
    DOUBLE
};


int handle_recv(int socket, struct InfoStruct info);


void
buildResponse(char** command, int item_count, char *response);

void getResponse(struct InfoStruct info, char* response);

int send_response(struct InfoStruct info, int socket);

char*
substring(char* string, int start, int end);

int isStringNumeric(char* string);

void printGamefield(char* gamefield);

extern enum numberwang numberwang;
