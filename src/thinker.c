#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

typedef struct
{
    int size;
    int* buffer;
} validPlacementStruct;

void
printField(char* field)
{
    // printf("  1 2 3 4 5 6 7 8");
    printf("  ___________________\n");
    for (int i = 0; i < 8; i++) {
        // int y = (i / 8) + 1;

        printf("%d | %c %c %c %c %c %c %c %c | %d\n",
               8 - i,
               field[i * 8 + 0],
               field[i * 8 + 1],
               field[i * 8 + 2],
               field[i * 8 + 3],
               field[i * 8 + 4],
               field[i * 8 + 5],
               field[i * 8 + 6],
               field[i * 8 + 7],
               8 - i);
    }
}
void
printValidPlacements(validPlacementStruct placements)
{
    printf("size: %d [ ", placements.size);

    for (int i = 0; i < placements.size; i++) {
        printf("%d, ", placements.buffer[i]);
    }

    printf("]\n");
}

int
calculateFieldScore(char* field, char player)
{
    // loop trough field and get the white score if isMaximisingPlayer or the
    // black score if not

    int playerScore = 0;
    for (int i = 0; i < 63; i++) {
        if (field[i] == player) {
            playerScore = playerScore + 1;
        }
    }

    return playerScore;
}

// void cpyField(int *src, int *dest)
// {
//     // memcpy(src, );
//     // for (size_t i = 0; i < 63; i++)
//     // {
//     //     dest[i] = src[i];
//     // }
// }

int
addPlacementBufferItem(validPlacementStruct* placementBuffer, int item)
{
    // printf("placement_buff_size: %d, item: %d\n", placementBuffer->size,
    // item);
    placementBuffer->buffer =
      realloc(placementBuffer->buffer,
              ((placementBuffer->size * sizeof(int)) + sizeof(int)));
    placementBuffer->buffer[placementBuffer->size] = item;
    placementBuffer->size = placementBuffer->size + 1;
    return 0;
}

int
getIndexByCoordinates(int x, int y)
{
    return (y - 1) * 8 + (x - 1);
}

int
getVectorScore(int x_rel,
               int y_rel,
               int x_vec,
               int y_vec,
               int distance,
               char stone,
               int score,
               char* field)
{
    int index = getIndexByCoordinates(x_rel, y_rel);
    if (index < 0 || index > 63) {
        return 0;
    }

    char ptr_val = field[index];
    if (ptr_val == '*') {
        return 0;
    }

    if (distance == 0 && ptr_val == stone) {
        return 0;
    }

    if (distance > 0 && ptr_val == stone) {
        return score;
    }

    if (ptr_val != stone && ptr_val != '*') {
        if (x_rel + x_vec > 8 || x_rel + x_vec < 1 || y_rel + y_vec > 8 ||
            y_rel + y_vec < 1 || (x_vec == 0 && y_vec == 0)) {
            return 0;
        }

        return getVectorScore((x_rel + x_vec),
                              (y_rel + y_vec),
                              x_vec,
                              y_vec,
                              distance + 1,
                              stone,
                              score + 1,
                              field);
    }
    return 0;
}

void
flipVectorEnemies(int x_rel,
                  int y_rel,
                  int x_vec,
                  int y_vec,
                  char stone,
                  char* field)
{
    int index = getIndexByCoordinates(x_rel, y_rel);

    if (field[index] == stone || x_rel < 1 || x_rel > 8 || y_rel < 1 ||
        y_rel > 8) {
        return;
    }

    if (field[index] != stone && field[index] != 0) {
        field[index] = stone;
        flipVectorEnemies(
          (x_rel + x_vec), (y_rel + y_vec), x_vec, y_vec, stone, field);
    }
}

bool
isValidPlacement(char* field, int x, int y, char stone)
{
    // printField(field);
    // printf("%c\n", stone);
    int index = getIndexByCoordinates(x, y);

    // printf("%c", field[index]);
    if (field[index] != '*') {
        return false;
    }
    // return false;

    bool encapsulatesEnemyStone = false;

    for (int x_vec = -1; x_vec <= 1; x_vec++) {
        for (int y_vec = -1; y_vec <= 1; y_vec++) {
            // Check out of bounds
            if (x + x_vec > 8 || x + x_vec < 1 || y + y_vec > 8 ||
                y + y_vec < 1 || (x_vec == 0 && y_vec == 0)) {
                continue;
            }

            if (getVectorScore(
                  (x + x_vec), (y + y_vec), x_vec, y_vec, 0, stone, 0, field) >
                0) {
                encapsulatesEnemyStone = true;
            }
        }
    }

    return encapsulatesEnemyStone;
}

void
getValidPlacements(validPlacementStruct* placementBuffer,
                   char* field,
                   char stone)
{

    for (int x = 1; x <= 8; x++) {
        for (int y = 1; y <= 8; y++) {

            if (isValidPlacement(field, x, y, stone)) {
                // printf("x:%d y:%d with index: %s is a valid placement for
                // stone: %c\n", x, y,
                //        getIndexByCoordinates(x, y), stone);
                addPlacementBufferItem(placementBuffer,
                                       getIndexByCoordinates(x, y));
            }
        }
    }
}

int
placeStoneInField(char* field, int index, int stone)
{
    int x = (index % 8) + 1;
    int y = (index / 8) + 1;

    field[getIndexByCoordinates(x, y)] = stone;

    for (int x_vec = -1; x_vec <= 1; x_vec++) {
        for (int y_vec = -1; y_vec <= 1; y_vec++) {
            if (x + x_vec > 8 || x + x_vec < 1 || y + y_vec > 8 ||
                y + y_vec < 1 || (x_vec == 0 && y_vec == 0)) {
                continue;
            }

            int score = getVectorScore(
              (x + x_vec), (y + y_vec), x_vec, y_vec, 0, stone, 0, field);

            if (score > 0) {
                // printf("x_vec: %d, y_vec: %d has score %d\n", x_vec, y_vec,
                // score);
                flipVectorEnemies(
                  (x + x_vec), (y + y_vec), x_vec, y_vec, stone, field);
                // printField(field);
            }
        }
    }

    return 0;
}

int
placeStone(char* field, int x, int y, char stone)
{
    if (isValidPlacement(field, x, y, stone)) {
        placeStoneInField(field, getIndexByCoordinates(x, y), stone);
    } else {
        // printf("Invalid placement: [%d, %d] stone: %c\n", x, y, stone);
        return -1;
        // printField(field);
    }
    return 0;
}

int
minimax(char* field,
        int lookaheads,
        int depth,
        bool isMaximisingPlayer,
        char fieldIndex,
        char maximizingStone,
        int minimizingStone)
{

    char currentPlayer;
    if (isMaximisingPlayer) {
        currentPlayer = maximizingStone;
    } else {
        currentPlayer = minimizingStone;
    }

    char* local_field = malloc(64);

    if (local_field == NULL) {
        // printf("asdfasdffasf");
        perror("malloc");
    }

    memcpy(local_field, field, 64);
    // printField(local_field);

    // printf("%d %d", field, local_field);

    if (depth > lookaheads) {
        // printField(local_field);
        int score = calculateFieldScore(local_field, currentPlayer);
        free(local_field);
        return score;
    }

    int x = (fieldIndex % 8) + 1;
    int y = (fieldIndex / 8) + 1;

    int ad = placeStone(local_field, x, y, currentPlayer);
    if (ad < 0) {
        printf("ELEOEOEOEOEOEOEOEEOEO");
    }

    // printf("[%d, %d] %c\n", x, y, currentPlayer);

    validPlacementStruct validPlacementRecord = { 0, malloc(0) };
    if (isMaximisingPlayer) {
        getValidPlacements(&validPlacementRecord, local_field, minimizingStone);
    } else {
        getValidPlacements(&validPlacementRecord, local_field, maximizingStone);
    }

    if (validPlacementRecord.size == 0) {
        printf("No valid moves left");
        free(validPlacementRecord.buffer);
        int score = calculateFieldScore(local_field, currentPlayer);
        free(local_field);
        return score;
    }

    if (isMaximisingPlayer) {
        int maxScore = -100000000;
        for (int i = 0; i < validPlacementRecord.size; i++) {
            int score = minimax(local_field,
                                lookaheads,
                                depth + 1,
                                false,
                                validPlacementRecord.buffer[i],
                                maximizingStone,
                                minimizingStone);
            if (score > maxScore) {
                maxScore = score;
            }
        }
        free(validPlacementRecord.buffer);
        free(local_field);
        return maxScore;
    } else {
        int minScore = 100000000;
        for (int i = 0; i < validPlacementRecord.size; i++) {

            int score = minimax(local_field,
                                lookaheads,
                                depth + 1,
                                true,
                                validPlacementRecord.buffer[i],
                                maximizingStone,
                                minimizingStone);
            if (score < minScore) {
                minScore = score;
            }
        }
        free(validPlacementRecord.buffer);
        free(local_field);
        return minScore;
    }
}

char*
think(char* field, char own_player)
{

    char x_coordinate_dict[8] = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H' };

    validPlacementStruct validPlacementRecord = { 0, malloc(0) };

    getValidPlacements(&validPlacementRecord, field, own_player);

    char enemy_player = 'W';
    if (own_player == 'W') {
        enemy_player = 'B';
    }
    // printf("buffer size: %d\n", validPlacementRecord.size);
    // printf("best field: [%d, %d]", x, y);

    // return validPlacementRecord.buffer[0];

    int best_field;
    int max_score = -10000000;
    for (int i = 0; i < validPlacementRecord.size; i++) {

        int score = minimax(field,
                            3,
                            0,
                            true,
                            validPlacementRecord.buffer[i],
                            own_player,
                            enemy_player);
        if (score > max_score) {
            best_field = validPlacementRecord.buffer[i];
            max_score = score;
        }
    }

    char* move = malloc(3);
    printValidPlacements(validPlacementRecord);
    printf("best field is: %d with score: %d\n", best_field, max_score);
    printf(": %d\n", best_field);
    int x = (best_field % 8) + 1;
    int y = (best_field / 8) + 1;

    // char y_move = (9 - y) + '0';
    sprintf(move, "%c%i", x_coordinate_dict[x - 1], (9 - y));

    // move[0] = x_coordinate_dict[x - 1];
    // move[1] = y_move;
    // move[2] = '\0';
    // free(validPlacementRecord.buffer);
    return move;
    // // free(&validPlacementRecord);
    // int x = (best_field % 8) + 1;
    // int y = (best_field / 8) + 1;
    // printf("best field: [%d, %d]", x, y);
    // free(validPlacementRecord.buffer);
    // return best_field;
}
