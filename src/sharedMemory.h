// guard
#ifndef _SHAREDMEMORY_H
#define _SHAREDMEMORY_H
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<string.h>
#include<stdbool.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/shm.h>



#define BUF_SIZE 64

#ifndef EXTERN
#define EXTERN extern
#endif

EXTERN int shmid;

struct spieler {
    int spielerNummer;
    char spielerName[BUF_SIZE];
    int ready_flag;
    struct spieler *pPlayer;
};

struct information {
        int playerQuantity;
        pid_t pid;
        pid_t ppid;
        int spielerNummer;
        char spielName[BUF_SIZE];
        struct spieler *pointerPlayer;
        int thinker_flag;
        char gamefield[65];
        char farbe;
    };



struct information* sharedMemory();

int deleteSharedMemory(int shmid);

#endif
