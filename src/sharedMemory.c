#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define EXTERN
#include "sharedMemory.h"

#define BUF_SIZE 64
#define MAX_PLAYERS 100

struct information *sharedMemory()
{
    struct information info;
    struct spieler player;

    info.spielerNummer = 1;
    player.spielerNummer = 1;

    const int SHMSIZE = (sizeof(info) + 100 * sizeof(player)); // minimal size of the shm
    // int shmid; // shm id
    struct information *shm; // pointer to shm
    key_t key = IPC_PRIVATE; // shm key

    // creates a shm segment with the key 'key'
    // IPC_CREATE: creates a new key
    // shmget returns id of the shm
    shmid = shmget(key, SHMSIZE, IPC_CREAT | 0666);
    // catch errors
    if (shmid == -1)
    {
        perror("shmget");
        exit(EXIT_FAILURE);
    }
    // attach shm
    shm = shmat(shmid, NULL, 0);
    if (shm == (void *)-1)
    {
        perror("shmat");
        exit(EXIT_FAILURE);
    }

    // store structure in shm
    //*shm = info;
    // shm++
    //*shm = spieler;
    return shm;
}

int deleteSharedMemory(int shmid)
{
    if (shmctl(shmid, IPC_RMID, NULL) == -1)
    {
        perror("shmctl");
        exit(EXIT_FAILURE);
    }
    return 1;
}

/*int main (void){
    struct information info;
    //info.playerQuantity = player_count;
    struct spieler player;
    //player.spielerNummer = 1;
    //int ready_flag = 1;
    sharedMemory(info, player);
}*/

// see the shared memory: ipcs -a
// delete: ipcrm -a

// zwei shared memory erstellen und zweites am ersten attachen
// zahl festlegen für maximale Anzahl Spieler und testen, ob die Spieleranzahl noch ins shared memory passt
// structures für Spieler in Array speichern wobei jedes Feld eine Struct ist

/* info.playerQuantity = 3;
    info.pid = 1;
    info.ppid = 2;
    info.spielerNummer = 7;
    char name[] = "blabla\0";
    strcpy(info.spielName, name);
    */
