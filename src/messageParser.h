/*Message Parser module header*/

#ifndef EXTERN
#define EXTERN extern
#endif

#define BUFF_SIZE 64
#define MAX_PLAYERS 100

EXTERN double msgp_server_version;
EXTERN char msgp_gamekind[BUFF_SIZE];
EXTERN char msgp_gamename[BUFF_SIZE];
EXTERN int msgp_player_number[MAX_PLAYERS];
EXTERN char msgp_player_name[MAX_PLAYERS][BUFF_SIZE];
EXTERN int msgp_players_total;
EXTERN int msgp_player_ready[MAX_PLAYERS];
EXTERN int msgp_time_left;
EXTERN int msgp_rows;
EXTERN int msgp_columns;
EXTERN int msgp_line_number;
EXTERN char msgp_line[8];
EXTERN char msgp_p0_won[BUFF_SIZE];
EXTERN char msgp_p1_won[BUFF_SIZE];
EXTERN char msgp_error_message[BUFF_SIZE];
EXTERN char msgp_gamefield[64];


void printServerMessage(char* message);
