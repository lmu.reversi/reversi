
#include "config.h"
#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

FILE*
openFile(char* name)
{
    char dateiName[256];
    FILE* datei = NULL;

    if (name == NULL) {
        printf("Bitte gib einen Dateinamen an: ");
        scanf("%s", dateiName);
    } else {
        strncpy(dateiName, name, 255 * sizeof(char));
    }

    if ((datei = fopen(dateiName, "r")) == NULL) {
        printf("Fehler beim Oeffnen der Datei!\n");
    }

    return datei;
}

config
fetchConfig()
{

    config configuration;

    FILE* datei = openFile("./client.conf");
    if (datei == NULL)
        exit(-1);
    char line[256];

    // lese Datei zeilenweise ein
    while (fgets(line, 256, datei) != NULL) {
        char* ptr = strtok(line, "=");

        int i = 0;
        char* key_value[2];

        while (ptr != NULL) {
            // sscanf(ptr, "%s\0", key_value[i++]);
            key_value[i++] = ptr;
            // key_value[i++][strlen(ptr)] = '\0';
            ptr = strtok(NULL, "=");
        }

        // scanf(key_value[1], "%s\n", configuration.host);
        if (strcmp(key_value[0], "host") == 0) {
            configuration.host = strtok(strdup(key_value[1]), "\n");
        } else if (strcmp(key_value[0], "port") == 0) {
            configuration.port = atoi(strdup(key_value[1]));
        } else if (strcmp(key_value[0], "type") == 0) {
            configuration.type = strtok(strdup(key_value[1]), "\n");
        } else if (strcmp(key_value[0], "maxPlayerCount") == 0) {
            configuration.max_player_count = atoi(strdup(key_value[1]));
            // sscanf(key_value[1], "%s\n", configuration.max_player_count);
        }
    }

    // Datei schliessen und die Anzahl der gelesenen Zeilen zurueck geben
    fclose(datei);
    return configuration;
}