/*
The Message Parser converts every possible Server-Message (currently only
Prolog-phase) into a more readable/ user-friendly format and provides a function
to print the message to the standart-output

Please use the printServerMessage method to print the recieved information from
the server A more detailed documentation and usage details of that function can
be found below, at the function declaration.
*/

#include <ctype.h>
#include <regex.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#define BUFF_SIZE 64
#define MAX_PLAYERS 100

#define EXTERN
#include "helper.h"
#include "messageParser.h"
#include "stateHandler.h"

char*
isReadyBool(char* b, int state)
{
    if (state == P_READY) {
        if (b[0] == '0') {
            return "not ready";
        } else {
            return "ready";
        }
    }
    return b;
}

void
matchMessage(char* buffer,
             char* message,
             char* regex_msg,
             char* format,
             int arg_count,
             ...)
{
    va_list valist;
    regex_t regex;
    regmatch_t arg_pos[arg_count + 1];
    int match;

    va_start(valist, arg_count);
    // printf("Try to match: \"%s\" with \"%s\"\n", message, regex_msg);
    match = regcomp(&regex, regex_msg, REG_EXTENDED);
    if (match) {
        regerror(match, &regex, buffer, sizeof(buffer));
    } else {
        match = regexec(&regex, message, arg_count + 1, arg_pos, 0);
        if (match == REG_NOMATCH) {
            sprintf(buffer, "Regex no match");
        } else if (!match) {
            char* opt_args[arg_count];
            char local_args[arg_count][BUFF_SIZE];
            for (int i = 0; i < arg_count; i++) {
                opt_args[i] = substring(
                  message, arg_pos[i + 1].rm_so, arg_pos[i + 1].rm_eo);
                strcpy(local_args[i], opt_args[i]);
                // sprintf(local_args[i], "%s", opt_args[i]);
                if (isStringNumeric(local_args[i]) == INTEGER) {
                    int* new_pointer = va_arg(valist, int*);
                    // printf("Working on argument %p\n", new_pointer);
                    int new_value = atoi(local_args[i]);
                    *new_pointer = new_value;
                    // printf("Wrote %i into %p\n", *new_pointer, new_pointer);
                    // printf("New VALUE: %i\n", new_value);
                } else if (isStringNumeric(local_args[i]) == DOUBLE) {
                    double* new_pointer = va_arg(valist, double*);
                    // printf("Working on argument %p\n", new_pointer);
                    double new_value = atof(local_args[i]);
                    *new_pointer = new_value;
                    // printf("Wrote %f into %p\n", *new_pointer, new_pointer);
                    // printf("New DOUBLE_VAL: %lf\n", new_value);
                } else {
                    char* new_pointer = va_arg(valist, char*);
                    // printf("Working on argument %p\n", new_pointer);
                    // har local_string[BUFF_SIZE];
                    strcpy(new_pointer, local_args[i]);
                    // printf("Wrote %s into %p\n", local_string, new_pointer);
                    // printf("New STRING: %s\n", local_string);
                }
                // local_args[i][strlen(opt_args[i])] = '\0';
                // strcat(local_args[i], "\0");
            }
            sprintf(buffer,
                    format,
                    local_args[0],
                    local_args[1],
                    isReadyBool(local_args[2], getCurrentState()),
                    local_args[3],
                    local_args[4],
                    local_args[5],
                    local_args[6],
                    local_args[7],
                    local_args[8]);
            for (int i = 0; i < arg_count; i++) {
                free(opt_args[i]);
            }
        } else {
            regerror(match, &regex, buffer, sizeof(buffer));
        }
    }
    va_end(valist);
    regfree(&regex);
}

int ready_index = 1;
int line_count = 0;
int gameover_flag = 0;

int
parseMessage(char* buffer, char* message)
{
    if (message[0] == '-') {
        updateState(NEGATIVE);
    }
    if (getCurrentState() == G_IDLE) {
        if (strcmp(message, "+ WAIT") == 0) {
            updateState(G_WAIT);
        }
        char* is_move = substring(message, 0, 6);
        if (strcmp(is_move, "+ MOVE") == 0) {
            updateState(G_MOVE);
        }
        free(is_move);
        if (strcmp(message, "+ GAMEOVER") == 0) {
            updateState(G_GAMEOVER);
        }
    }
    switch (getCurrentState()) {
        case P_WELCOME:
            matchMessage(buffer,
                         message,
                         "\\+ MNM Gameserver v([[:digit:][:punct:][:digit:]]+)",
                         "Welcome to the MNM Server - Version %s - Waiting "
                         "for connections...",
                         1,
                         &msgp_server_version);
            updateState(getCurrentState() + 1);
            break;
        case P_CLIENT:
            matchMessage(
              buffer,
              message,
              "\\+ Client version accepted - please send Game-ID to join",
              "Your client version has been accepted - Please send your "
              "Game ID to join.",
              0);
            updateState(getCurrentState() + 1);
            break;
        case P_GAMEKIND:
            matchMessage(buffer,
                         message,
                         "\\+ PLAYING (\\w+)",
                         "You are now playing: %s",
                         1,
                         &msgp_gamekind);
            updateState(getCurrentState() + 1);
            break;
        case P_GAMENAME:
            matchMessage(
              buffer, message, "\\+ (.+)", "Game name: %s", 1, &msgp_gamename);
            // printf("Gamename test: %s\n", msgp_gamename);
            updateState(getCurrentState() + 1);
            break;
        case P_YOU:
            matchMessage(buffer,
                         message,
                         "\\+ YOU ([[:digit:]]+) (.+[^[:space:]])",
                         "Playing as Player #%s - %s",
                         2,
                         &msgp_player_number[0],
                         msgp_player_name[0]);
            // printf("PLAYER TEST: %s\n", msgp_player_name[0]);
            // printf("PLAYER NUMBER TEST: %i\n", msgp_player_number[0]);
            updateState(getCurrentState() + 1);
            break;
        case P_TOTAL:
            matchMessage(buffer,
                         message,
                         "\\+ TOTAL ([[:digit:]])",
                         "Total amount of players: %s",
                         1,
                         &msgp_players_total);
            updateState(getCurrentState() + 1);
            break;
        case P_READY:
            matchMessage(buffer,
                         message,
                         "\\+ ([[:digit:]]+) (.+[^[:space:]]) ([[:digit:]])",
                         "Player #%s %s is %s",
                         3,
                         &msgp_player_number[ready_index],
                         msgp_player_name[ready_index],
                         &msgp_player_ready[ready_index]);
            ready_index++;
            if (msgp_players_total == ready_index) {
                updateState(getCurrentState() + 1);
            }
            break;
        case P_ENDPLAYERS:
            matchMessage(
              buffer, message, "\\+ ENDPLAYERS", "Starting Game...", 0);
            updateState(G_IDLE);
            break;
        case G_WAIT:
            matchMessage(buffer, message, "\\+ WAIT", "Please Wait", 0);
            updateState(G_IDLE);
            break;
        case G_MOVE:
            matchMessage(buffer,
                         message,
                         "\\+ MOVE ([[:digit:]]+)",
                         "You have %s ms to send a valid move!",
                         1,
                         &msgp_time_left);
            updateState(getCurrentState() + 1);
            break;
        case G_FIELD:
            matchMessage(buffer,
                         message,
                         "\\+ FIELD ([[:digit:]]+),([[:digit:]]+)",
                         "The current gamefield consists of %s columns %s rows",
                         2,
                         &msgp_columns,
                         &msgp_rows);
            updateState(getCurrentState() + 1);
            break;
        case G_LINE:
            if (line_count == 0) {
                strcpy(msgp_gamefield, "");
            }
            matchMessage(buffer,
                         message,
                         "\\+ ([[:digit:]]) ([\\*WB]) ([\\*WB]) ([\\*WB]) "
                         "([\\*WB]) ([\\*WB]) ([\\*WB]) ([\\*WB]) ([\\*WB])",
                         "%s. | %s %s  %s  %s  %s  %s  %s  %s",
                         9,
                         &msgp_line_number,
                         &msgp_line[0],
                         &msgp_line[1],
                         &msgp_line[2],
                         &msgp_line[3],
                         &msgp_line[4],
                         &msgp_line[5],
                         &msgp_line[6],
                         &msgp_line[7]);
            line_count++;
            strcat(msgp_gamefield, msgp_line);
            if (msgp_rows == line_count) {
                line_count = 0;
                updateState(getCurrentState() + 1);
            }
            break;
        case G_ENDFIELD:
            // printGamefield(msgp_gamefield);
            matchMessage(buffer,
                         message,
                         "\\+ ENDFIELD",
                         "Gamefield drawn completely. Please confirm THINKING",
                         0);
            if (gameover_flag) {
                updateState(G_P0WON);
            } else {
                updateState(getCurrentState() + 1);
            }
            break;
        case G_OKTHINK:
            matchMessage(buffer,
                         message,
                         "\\+ OKTHINK",
                         "Thinking-notification recieved.You can now send "
                         "your move.",
                         0);
            updateState(M_MOVEOK);
            break;
        case G_GAMEOVER:
            matchMessage(
              buffer, message, "\\+ GAMEOVER", "### Game Over ###", 0);
            gameover_flag = 1;
            updateState(G_FIELD);
            break;
        case G_P0WON:
            matchMessage(buffer,
                         message,
                         "\\+ PLAYER0WON (Yes|No)",
                         "Player 0 winnner? %s",
                         1,
                         &msgp_p0_won);
            updateState(getCurrentState() + 1);
            break;
        case G_P1WON:
            matchMessage(buffer,
                         message,
                         "\\+ PLAYER1WON (Yes|No)",
                         "Player 1 winnner? %s",
                         1,
                         &msgp_p1_won);
            updateState(getCurrentState() + 1);
            break;
        case G_QUIT:
            matchMessage(buffer,
                         message,
                         "\\+ QUIT",
                         "Thanks for playing :) - Closing connection...",
                         0);
            break;
        case M_MOVEOK:
            matchMessage(
              buffer, message, "\\+ MOVEOK", "Your move was accepted", 0);
            updateState(G_IDLE);
            break;
        case NEGATIVE:
            matchMessage(
              buffer,
              message,
              "- (.+)",
              "(!) An Error occurred. (Msg.: %s) - Closing connection...",
              1,
              &msgp_error_message);
            break;
        default:
            return -1;
    }
    return 0;
}

void
printServerMessage(char* message)
{
    char* buffer = malloc(BUFF_SIZE * 2);
    parseMessage(buffer, message);
    printf("S: %s\n", buffer);
    free(buffer);
}

/*Use a main function to test the possible cases, when adding new ones.*/

/*
int main(int argc, char** argv){
    printServerMessage("+ MNM Gameserver 2.4 accepting connections");
    printServerMessage("+ Client version accepted - please send Game-ID to
join"); printServerMessage("+ PLAYING Reversi"); printServerMessage("+
R3v3rs1Pr0xXx69"); printServerMessage("+ YOU 12 R3v3rs1Pr0xXx69");
    printServerMessage("+ TOTAL 3");
    printServerMessage("+ 12345678 R3v3rs1Pr0xXx69 1");
    printServerMessage("+ 11019876 R3v3rs1Pr0xXx68 0");
    printServerMessage("+ 55555555 GAMEOVER 1");
    printServerMessage("+ ENDPLAYERS\n");
    printServerMessage("+ WAIT");
    printServerMessage("+ MOVE 24632");
    printServerMessage("+ FIELD 8,8");
    printServerMessage("+ 1 * * W B * B * *");
    printServerMessage("+ 2 W * W B * B * *");
    printServerMessage("+ 3 * * W B * B * B");
    printServerMessage("+ 4 * W W B B B * *");
    printServerMessage("+ 5 * W W B * B * *");
    printServerMessage("+ 6 * * W B * B * *");
    printServerMessage("+ 7 * W * B * B * *");
    printServerMessage("+ 8 * W W B * B * *");
    printf("%Gamefield: %s\n", msgp_gamefield);
    printServerMessage("+ ENDFIELD\n");
    printServerMessage("+ OKTHINK\n");
    printServerMessage("+ MOVEOK\n");
    printServerMessage("+ GAMEOVER\n");
    printServerMessage("+ 1 * * W B * B * *\n");
    printServerMessage("+ 2 W * W B * B * *\n");
    printServerMessage("+ 3 * * W W W B * B\n");
    printServerMessage("+ 4 * W W B B B * *\n");
    printServerMessage("+ 5 * W W W W B W W\n");
    printServerMessage("+ 6 * * W B W B W W\n");
    printServerMessage("+ 7 * W * B W W * *\n");
    printServerMessage("+ 8 * W W B * B * *\n");
    printServerMessage("+ PLAYER0WON Yes\n");
    printServerMessage("+ PLAYER1WON No\n");
    printServerMessage("+ QUIT\n");
    //printServerMessage("- TIMEOUT\n");
    return 0;
}
*/
