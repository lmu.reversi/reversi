#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "helper.h"

#define BUF 64

#define VERSION "2.42"

int
performConnection(int sock, struct sockaddr_in server)
{
    // connect the soccet to the server
    if (connect(sock, (struct sockaddr*)&server, sizeof(server)) == 0) {
        printf("I: Connected to %s\n", inet_ntoa(server.sin_addr));
    } else {
        perror("connect");
        exit(EXIT_FAILURE);
    }



    /*char* command1[2] = { "VERSION", VERSION };
    send_command(command1, 2, sock);

    if (handle_recv(buffer, sock) < 0)
    {
        printf("I: Exiting program due to unmet dependencies.\n");
        exit(EXIT_FAILURE);
    };

    char *command2[2] = {"ID", game_id};
    send_command(command2, 2, sock);

    if (handle_recv(buffer, sock) < 0)
    {
        printf("I: Could be an invalid game ID. Please check if the entered ID "
               "is correct.\n");
        exit(EXIT_FAILURE);
    };

    // Convert player count integer to string to build the command correctly

    if (player_count == -1)
    {
        char *command3[1] = {"PLAYER"};
        send_command(command3, 1, sock);
    }
    else
    {
        char player_count_str[12];
        sprintf(player_count_str, "%d", player_count);
        char *command3[2] = {"PLAYER", player_count_str};
        send_command(command3, 2, sock);
    }*/

    // if (handle_recv(buffer, sock) < 0) {
    //     exit(EXIT_FAILURE);
    // };

    return 1;
};
