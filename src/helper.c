#include <arpa/inet.h>
#include <ctype.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "messageParser.h"
#include "stateHandler.h"

#include "helper.h"

#define BUF 64


int
handle_recv(int socket, struct InfoStruct info)
{
    char* message = calloc(BUF, sizeof(char));
    char buffer[2];
    ssize_t size;

    buffer[0] = 'x';

    while(buffer[0] != '\n'){
        strcpy(buffer,  "");
        size = recv(socket, buffer, 1, 0);
        if(size < 0){
            perror("Failed to recieve message!\n");
            return -1;
        }
        buffer[1] = '\0';
        strcat(message, buffer);
    }


    if(message[0] == '-'){
        printServerMessage(message);
        return 0;
    }

    message[strlen(message)-1] = '\0';

    //printf("%s\n", message);
    printServerMessage(message);

    if(getCurrentState() == G_QUIT){
        return 0;
    }

    // try to send a response
    send_response(info, socket);
    free(message);
    return 1;
}

void
buildResponse(char** command, int item_count, char *response)
{
    int total_chars = 0;
    for (int x = 0; x < item_count; x++) {
        total_chars += strlen(command[x]);
    }

    char msg[total_chars + item_count+2]; 
    for (int i = 0; i < item_count; i++) {
        if (i == 0) {
            strcpy(msg, command[i]);
            continue;
        }
        if (i > 0) {
            strcat(msg, " ");
        }
        strcat(msg, command[i]);
    }
    strcat(msg, "\n\0");
    strcpy(response, msg);
}

void
getResponse(struct InfoStruct info, char *response)
{
    char temp[BUF];
    char* resp_version[2] = { "VERSION", info.version };
    char* resp_game_id[2] = { "ID", info.game_id };
    char* resp_wait[1] = {"OKWAIT"};
    char* resp_thinking[1] = {"THINKING"};
    char* resp_move[2] = {"PLAY", info.next_move};


    switch (getPreviousState()) {
        case P_WELCOME:
            buildResponse(resp_version, 2, temp);
            break;
        case P_CLIENT:
            buildResponse(resp_game_id, 2, temp);
            break;
        case P_GAMENAME:
            {
            char** resp_player_num;
            if(strcmp(info.player_num, "") == 0){
                resp_player_num = malloc(1*sizeof(char**));
                resp_player_num[0] = "PLAYER";
            }
            else{
                resp_player_num = malloc(2*sizeof(char**));
                resp_player_num[0] = "PLAYER";
                resp_player_num[1] = info.player_num;
            }
            if(sizeof(resp_player_num) == 2*sizeof(char**)){
                buildResponse(resp_player_num, 2, temp);
            }
            else{
                buildResponse(resp_player_num, 1, temp);
            }
            free(resp_player_num);
            break;
            }
        case G_ENDFIELD:
            buildResponse(resp_thinking, 1, temp);
            break;
        case G_OKTHINK:
            buildResponse(resp_move, 2, temp);
            break;
        case G_WAIT:
            buildResponse(resp_wait, 1, temp);
            break;
        default:
            strcpy(temp, "");
        }
        strcpy(response, temp);
}

int
send_response(struct InfoStruct info, int socket)
{
    char response[BUF];
    getResponse(info, response);

    //int bytes_to_send = strlen(response);

    if(strcmp(response, "") != 0){
        printf("C: %s", response);
        send(socket, response, strlen(response), 0);
    }
    return 0;
};

char*
substring(char* string, int start, int end)
{
    char* buffer = malloc(end - start + 1); 
    
    for (int i = start; i < end; i++) {
        buffer[i - start] = string[i];
    }
    buffer[end - start] = '\0';
    return buffer;
}

int
isStringNumeric(char* string)
{
    int result = NON_NUMERIC;
    int double_flag = 0;

    for (int i = 0; i < (int)strlen(string); i++) {
        if (isdigit(string[i]) && !double_flag) {
            result = INTEGER;
        } else if ((string[i] == '.' && !double_flag) ||
                   (isdigit(string[i]) && double_flag)) {
            result = DOUBLE;
            double_flag = 1;
        } else {
            result = NON_NUMERIC;
            break;
        }
    }
    return result;
}

void printGamefield(char* gamefield){
    printf("   A B C D E F G H   \n");
    printf(" +-----------------+ \n");
    for(int i = 0; i<64; i++){
        if(i%8 == 0){
            char* line = substring(gamefield, i, i+8);
            printf("%i| ", ((int)i/8)+1);
            for(int j = 0; j<8; j++){
                printf("%c ", line[j]);
            }
            printf("|\n");
            free(line);
        }
    }
    printf(" +-----------------+ \n");

}
