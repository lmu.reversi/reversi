#define _POSIX_C_SOURCE 200809L
#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#define _GNU_SOURCE

#include "config.h"
#include "performConnection.h"

#define EXTERN
#include "helper.h"
#include "messageParser.h"
#include "sharedMemory.h"
#include "stateHandler.h"
#include "thinker.h"

#define BUF 64
#define BUF_SIZE 64

// Flag for the signal-handler
sig_atomic_t got_usr1;

// signal-handler (sets flag to 1)
void
sigusr1_handler()
{
    got_usr1 = 1;
}

int
main(int argc, char** argv)
{
    // store player count locally
    int desired_player_num = -1;
    // store the game id locally
    char game_id[14];

    // scan console input if argc == 1
    char input[10];

    struct InfoStruct general_info;
    // print instructions if no console input was received and check if the game
    // should be implemented with default values
    if (argc == 1) {
        printf("Guidance for using this program:\n");
        printf("Please enter a game ID an your desired player count.\n");
        printf("Options available:\n");
        printf("sysprak-client <gameID> <playerCount> : Please enter a game ID "
               "with 13 digits and your desired player count.\n");
        printf("If you do not specify a game ID or your desired player count, "
               "the game will be implemented using default values.\n");
        printf("If you want to continue this game without specifying a game ID "
               "or your player count, please enter 'continue', else end this "
               "program by typing Strg + C\n");
        scanf("%s", &input[0]);
        // Test value for comparing strings
        char test[] = "continue";
        // checking if the player wants to continue the game and ending the
        // program if not if the player wants to continue, then default values
        // for the game ID and the player count are implemented
        // TODO: enter existing game ID as default
        if (strcmp(input, test) == 0) {
            strcpy(game_id, "1234567891011"); // this game ID ist just a dummy!!
            desired_player_num = -1;
            printf("%s\n", game_id);
        } else {
            exit(EXIT_FAILURE);
        }
    }

    // check if we get the correct amount of arguments
    if (argc == 4) {
        printf(
          "E: Oops, looks like some arguments are missing. Please make sure "
          "your enter a valid game ID and your desired player count.\n");
        exit(EXIT_FAILURE);
    }

    config configuration = fetchConfig();
    // parse parameters from command line
    int ret;
    while ((ret = getopt(argc, argv, "g:p:")) != -1) {
        switch (ret) {
            case 'g':
                sprintf(game_id, "%s", optarg);
                break;
            case 'p':
                if (strcmp(optarg, "0") == 0 || strcmp(optarg, "1") == 0) {
                    desired_player_num = atoi(optarg);
                } else {
                    desired_player_num = -1;
                }
                break;
            default:
                break;
        }
    }

    // game_id = argv[1];
    if (strlen(game_id) != 13) {
        printf("E: The given game ID is invalid. It has to be 13 characters "
               "long.\n");
        exit(EXIT_FAILURE);
    }

    printf("I: Game ID: %s\n", game_id);

    // check if the player count is feasable since you can't really play alone
    // or against a negative amount of players
    if (desired_player_num > 1) {
        printf("E: Use 0 for Player 1 and 1 for Player 2.\n");
        exit(EXIT_FAILURE);
    }
    // fetch IP address of the given hostname by querying a DNS server

    struct hostent* hp = gethostbyname(configuration.host);

    if (hp == NULL) {
        perror("gethostbyname");
        printf("E: Could not resolve host.\n");
        exit(1);
    }

    struct sockaddr_in server;
    server.sin_family = AF_INET;
    server.sin_port = htons(configuration.port);
    server.sin_addr = *(struct in_addr*)hp->h_addr_list[0];

    // initialize a new socket instance in memory
    int sock = socket(AF_INET, SOCK_STREAM, 0);

    // perform the initial connection to the server
    // performConnection(game_id, desired_player_num, sock, server);

    struct information* shmPointer;
    shmPointer = sharedMemory();

    // initialize Singal-Handler and set flag to 0
    // void sigusr1_handler(int sig);
    got_usr1 = 0;

    // initialize sigaction structure
    struct sigaction signal;
    signal.sa_handler = sigusr1_handler;
    sigemptyset(&signal.sa_mask);
    signal.sa_flags = 0;

    // create the pipe
    int fd[2];

    if (pipe(fd) < 0) {
        perror("pipe");
        exit(EXIT_FAILURE);
    }

    // splitting into two processes thinker and connector
    pid_t pid;
    int returnStatus;
    struct information* infos = shmPointer;
    switch (pid = fork()) {
        case -1:
            perror("fork() failed.");
            exit(EXIT_FAILURE);
        case 0: // Thinker
            while (1) {
                // close the read side
                close(fd[0]);
                // handling the singal
                if (sigaction(SIGUSR1, &signal, NULL) == -1) {
                    perror("sigaction");
                    exit(1);
                }
                // waiting for the Signal
                pause();
                // Signal sets got_usr1 from 0 to 1
                if (got_usr1 == 1 && infos->thinker_flag == 1) {
                    printf("T: Writing game-move!\n");
                    char move_cpy[3] = "  ";
                    char* move = think(infos->gamefield, infos->farbe);
                    strcpy(move_cpy, move);
                    free(move);
                    printf("best move is: %s for player: %c",
                           move_cpy,
                           infos->farbe);

                    if (write(fd[1], move_cpy, sizeof(move_cpy)) == -1) {
                        perror("write");
                    }
                    // free(move);
                    printf("T: Done\n");
                }
            }
            break;
        default: // Connector
                 // perform the initial connection to the server //=
                 // malloc(BUF_SIZE*sizeof(char));
            performConnection(sock, server);
            general_info.game_id = game_id;
            general_info.version = "2.42";
            general_info.player_num = malloc(2); // wird in helper.c gefreed
            if (desired_player_num != -1) {
                sprintf(general_info.player_num, "%i", desired_player_num);
            } else {
                strcpy(general_info.player_num, "");
            }
            general_info.next_move = malloc(3);
            int result = 1;
            while (result) {
                result = handle_recv(sock, general_info);
                if (!result) {
                    printf("Time to go!\n");
                    kill(pid, SIGKILL);
                    exit(0);
                    break;
                }
                // printf("State: %i\n", getCurrentState());
                // close the write side
                close(fd[1]);
                // printf("Current State: %i\n", getCurrentState());
                // try to send a response
                // send_response(general_info, sock);
                // free(buffer);

                // initialize structures
                // Information structure
                infos->playerQuantity = msgp_players_total;
                infos->pid = pid;
                infos->ppid = getppid();
                infos->spielerNummer = msgp_player_number[0];
                char* name = msgp_gamename;
                strcpy(infos->spielName, name);
                if (infos->spielerNummer == 0) {
                    infos->farbe = 'B';
                } else {
                    infos->farbe = 'W';
                }
                infos->thinker_flag = 0;

                if (getCurrentState() == G_ENDFIELD) {
                    strcpy(infos->gamefield, msgp_gamefield);
                }

                // Player structure
                // initialize array and fill array with players
                struct spieler arraySpieler[3];
                for (int i = 0; i < msgp_players_total; i++) {
                    arraySpieler[i].spielerNummer = msgp_player_number[i];
                    char* nameSp = msgp_player_name[i]; // blabla\0";
                    strcpy(arraySpieler[i].spielerName, nameSp);
                    arraySpieler[i].ready_flag = msgp_player_ready[i];
                }
                // set pointer
                infos->pointerPlayer = &arraySpieler[0];

                // set flag to 1 and signalize thinker to stark thinking
                if (getCurrentState() == G_OKTHINK) {
                    infos->thinker_flag = 1;
                    // seng signal to thinker (sigusr1)
                    char buf[3] = "  ";
                    if (kill(pid, 10) == -1) {
                        perror("kill");
                    } else {
                        printf("T: Signal gesendet\n");
                    }

                    if (read(fd[0], buf, sizeof(buf)) == -1) {
                        perror("read");
                    } else {
                        printf("T: OUR MOVE: %s\n", buf);
                        strcpy(general_info.next_move, buf);
                    }
                    // free(buf);
                }
            }
            // free(configuration.host);
            // free(configuration.max_player_count);
            // free(configuration.port);
            // free(configuration.type);
            break;
    }

    waitpid(pid, &returnStatus, 0);
    if (returnStatus == 0) {
        printf("The connector terminated normally.\n");
    } else {
        printf("The connector terminated with an error!.\n");
    }

    free(general_info.next_move);
    deleteSharedMemory(shmid);
    exit(returnStatus);
    return 1;
}
/*
        // perform the initial connection to the server //=
   malloc(BUF_SIZE*sizeof(char)); performConnection(sock, server);
        general_info.game_id = game_id;
        general_info.version = "2.42";
        general_info.next_move = malloc(3);
        while (handle_recv(sock, general_info) == 1) {
            //printf("State: %i\n", getCurrentState());
            // close the write side
            close(fd[1]);
            //printf("Current State: %i\n", getCurrentState());
            // try to send a response
            //send_response(general_info, sock);
            // free(buffer);

            // initialize structures
            // Information structure
            infos->playerQuantity = msgp_players_total;
            infos->pid = pid;
            infos->ppid = getppid();
            infos->spielerNummer = msgp_player_number[0];
            char *name = msgp_gamename;
            infos->spielName = malloc(BUF_SIZE);
            strcpy(infos->spielName, name);
            infos->thinker_flag = 0;

            // Player structure
            // initialize array and fill array with players
            struct spieler arraySpieler[3];
            for (int i = 0; i < msgp_players_total; i++)
            {
                arraySpieler[i].spielerNummer = msgp_player_number[i];
                char *nameSp = msgp_player_name[i]; // blabla\0";
                arraySpieler[i].spielerName = malloc(BUF_SIZE);
                strcpy(arraySpieler[i].spielerName, nameSp);
                arraySpieler[i].ready_flag = msgp_player_ready[i];
            }
            // set pointer
            infos->pointerPlayer = &arraySpieler[0];

            pid_t ppid = getppid();
            // set flag to 1 and signalize thinker to stark thinking
            if(getCurrentState() == G_OKTHINK){
                infos->thinker_flag = 1;
                // seng signal to thinker (sigusr1)
                buf = malloc(3);
                if (kill(ppid, 10) == -1)
                {
                    perror("kill");
                }
                else
                {
                    printf("T: Signal gesendet\n");
                }

                if (read(fd[0], buf, sizeof(buf)) == -1)
                {
                    perror("read");
                }
                else
                {
                    printf("T: OUR MOVE: %s\n", buf);
                    strcpy(general_info.next_move, buf);
                }
                free(buf);
            }
            printf("Process ID: %i\nPaarent PRcess ID: %i", pid, ppid);
            if(getCurrentState() == NEGATIVE || getCurrentState() == G_QUIT){
                kill(pid, SIGKILL);
                free(general_info.next_move);
                exit(0);
                break;
            }
        }
        break;
*/
