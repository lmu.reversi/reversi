#include "stateHandler.h"

int current_state = P_WELCOME;
int previous_state;

int getCurrentState(){
	return current_state;
}

int getPreviousState(){
	return previous_state;
}

void updateState(int new_state) {
	int old_state = current_state;
	current_state = new_state;
	previous_state = old_state;
}
