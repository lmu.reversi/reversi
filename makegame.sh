#!/bin/bash
# VERSION: 1.1.0

make

## create a new game
ID=$(curl http://sysprak.priv.lab.nm.ifi.lmu.de/api/v1/matches \
-H "Content-Type: application/json" \
-X POST \
-d '{"type":"Reversi","gameGeneric":{"name":"","timeout":3000},"gameSpecific":{},"players":[{"name":"White Player","type":"COMPUTER"},{"name":"Black Player","type":"COMPUTER"}]}' 2>/dev/null | grep -Eow '([a-z0-9]{13})')

if [[ $ID != "" ]]; then
  echo "Generated new game with ID \"$ID\"."
else
  echo "Error creating new Game-ID. Exiting..."
  exit
fi

./build/sysprak-client -g $ID
